from flask import Flask, render_template
from newsapi import NewsApiClient

app = Flask(__name__)

@app.route('/')
def Index():
    newsapi = NewsApiClient(api_key="08514dc5e64b431a98ec6092f7ea320b")
    topheadlines = newsapi.get_top_headlines(sources="al-jazeera-english")

    articles = topheadlines['articles']

    desc = []
    news = []
    img = []
    url = []
    publAt = []

    for i in range(len(articles)):
        myarticles = articles[i]

        news.append(myarticles['title'])
        desc.append(myarticles['description'])
        img.append(myarticles['urlToImage'])
        url.append(myarticles['url'])
        publAt.append(myarticles['publishedAt'])
        

    mylist = zip(news, desc, img,url,publAt)

    return render_template('index.html', context = mylist)
@app.route('/bbc')
def bbc():
    newsapi = NewsApiClient(api_key="08514dc5e64b431a98ec6092f7ea320b")
    topheadlines = newsapi.get_top_headlines(sources="abc-news-au")

    articles = topheadlines['articles']

    desc = []
    news = []
    img = []
    url = []
    publAt = []

    for i in range(len(articles)):
        myarticles = articles[i]

        news.append(myarticles['title'])
        desc.append(myarticles['description'])
        img.append(myarticles['urlToImage'])
        url.append(myarticles['url'])
        publAt.append(myarticles['publishedAt'])

    mylist = zip(news, desc, img,url,publAt)


    return render_template('bbc.html', context = mylist)
    
@app.route('/fox')
def fox():
    newsapi = NewsApiClient(api_key="08514dc5e64b431a98ec6092f7ea320b")
    topheadlines = newsapi.get_top_headlines(sources="axios")

    articles = topheadlines['articles']

    desc = []
    news = []
    img = []
    url = []
    publAt = []

    for i in range(len(articles)):
        myarticles = articles[i]

        news.append(myarticles['title'])
        desc.append(myarticles['description'])
        img.append(myarticles['urlToImage'])
        url.append(myarticles['url'])
        publAt.append(myarticles['publishedAt'])

    mylist = zip(news, desc, img,url,publAt)


    return render_template('fox.html', context = mylist)
@app.route('/nbc')
def nbc():
    newsapi = NewsApiClient(api_key="08514dc5e64b431a98ec6092f7ea320b")
    topheadlines = newsapi.get_top_headlines(sources="ary-news")

    articles = topheadlines['articles']

    desc = []
    news = []
    img = []
    url = []
    publAt = []

    for i in range(len(articles)):
        myarticles = articles[i]

        news.append(myarticles['title'])
        desc.append(myarticles['description'])
        img.append(myarticles['urlToImage'])
        url.append(myarticles['url'])
        publAt.append(myarticles['publishedAt'])

    mylist = zip(news, desc, img,url,publAt)


    return render_template('nbc.html', context = mylist)
    
if __name__ == " __main__":
    app.debug = True
    app.run